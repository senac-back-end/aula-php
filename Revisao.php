<?php

$Nome = 'Marcelo';
$Idade = '24';
$Email = 'marcelo_castao@hotmail.com';
$Senha = '12345678';
$Cursos = ['PHP', 'HTML', 'CSS'];



echo '<h1>Trabalhando com Estrutura condicional</h1>';

echo '<h2>Exemplo de if (se...)</h2>';

if ($Idade >= 18) {
    echo "O usuário $Nome é maior de idade";
}
echo '<hr>';

###############################################

echo "<h2>Exemplo de if ternario</h2>";

echo ($Idade >= 18) ? "Maior de idade" : "Menor de idade";

##############################################

echo '<hr>';

echo '<h2>Exemplo de if e else</h2>';



if ($Email == 'marcelo_castao@hotmail.com' && $Senha == '12345678') {
    echo "Usuário Logado";
} else {
    echo 'Usuário ou senha inválidos';
}

echo "<h2>Exemplo Login de if e else</h2>";

if ($Email == 'marcelo_castao@hotmail.com') {
    if ($Senha == '12345678') {
        echo "Usuário Logado";
    } else {
        echo "Usuário ou senha Inválidos";
    }
}else {
        echo "Usuário ou senha inválidos";
    }

    #############################################
    echo "<hr>";

echo "<h2>Exemplo de Multiplas Condições</h2>";

$num1 = 10;
$num2 = 20;

if($num1 == $num2){
    echo "os numeros são iguais";
} 
elseif ($num1 > $num2) {
    echo "O numero 1 é maior que o numero 2";
}
 else {
    echo "O numero 2 é maior que o numero 1";
}
###############################################

echo "<hr>";

echo "<h2>Exemplo de GET</h2>";


$menu = $_GET['menu'] ?? "home";

switch(strtolower($menu)){
    case "home":
        echo "Página principal";
        break;
    case "empresa":
        echo "Pagina empresa";
        break;
    case "Produtos":
        echo "Pagina produtos";
        break;
    case "Contato":
        echo "Pagina contato";
        break;
    default:
        echo "Pagina de erro 404";

}

