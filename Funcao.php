<?php

 function soma($num1, $num2)
 {
    $total = $num1 + $num2;
    return $total;
 }

 echo soma(10, 5);

 echo "<hr>";

 function subtrair($num1, $num2)
 {
     $total = $num1 - $num2;
     return $total;
 }

 echo subtrair(20, 15);

 echo "<hr>";

 function multiplicar($num1, $num2)
 {
   $total = $num1 * $num2;
   return $total;
 }

 echo multiplicar(10, 6);

 echo "<hr>";

 function dividir($num1, $num2)
 {
    $total = $num1 / $num2;
    return $total;
 }

 echo dividir(60, 2);


 echo "<hr>";

 function par_ou_impar($num1)
 {
     if ($num1 % 2 ==0){
        return "par";
     }
     return "impar"; 
 }

 function par_ou_impar_mod2($numero){
    return($numero % 2 ==0)? "numero é par" : "numero é impar";

 }

 ///////////////////////

 echo "<br>";

 function geradordesenhacomfor($senhainicial, $senhafinal){

    for($contador = $senhainicial; $contador <= $senhafinal; $contador++){
        echo $contador, "-";

    }
 }
 geradordesenhacomfor(10,20);

 echo "<hr>";

 function geradordesenhacomwhile($senhainicial = 1, $senhafinal = 10){
     
    $contador = $senhainicial;

    while($contador <= $senhafinal){
        echo $contador, "-";
        $contador ++;

    }
 }
 geradordesenhacomwhile(10,20);




