<?php

$Nome = 'Marcelo Castão';
$Profissao = 'Docente';
$CPF = '472.336.478-10';
$RG = '53.088.624-8';
$Empresa = 'Kalunga';
$CNPJ = '43.283.811/0001-50';
$Endereco = 'Rua São Luis, 125';
$Cidade ='Marília';
$Estado ='SP';
$CEP = '1750-000';


?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>DECLARAÇÃO DE LOCAL DE TRABALHO</title>

</head>
<body>
    <h1>DECLARAÇÃO DE LOCAL DE TRABALHO</h1>

    <P>
    Eu, <?=$Nome?>, brasileiro, <?=$Profissao?>, inscrito(a) no CPF sob o nº <?=$CPF?> e no RG nº<?=$RG?> para os devidos fins que possuo vínculo empregatício com a empresa <?=$Empresa?> inscrita no CNPJ sobo nº <?=$CNPJ?>, localizada à <?=$Endereco?> , <?=$Cidade?>, <?=$Estado?>, <?=$CEP?>.
    </P>
    <p>
    Por ser expressão da verdade, firmo apresente para efeitos legais. 
    </p>
    <p>
    Marília–SP, 15 de Setembro de 2022 
    </p>
    <br>
    <p>
    <?=$Nome?>.
    </p>

</body>
</html>




